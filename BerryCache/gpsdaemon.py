import gps
import threading
import ConfigParser
 
# The daemon stores the data every second in these variables:
# Current LAT
currentLat = 0.0
# Current LON
currentLon = 0.0
# Direction in degrees
direction = 0.0

# Return a dict with the current GPS latitude and longitude + direction
def getlatandlon():
    config = ConfigParser.RawConfigParser()
    # if the GPS has signal
    if currentLat != 0.0 and currentLon != 0.0 and direction != 0.0:
        cLon = currentLon
        cLat = currentLat
        direc = direction
        
        config.add_section('S1')
        config.set('S1', 'currentLon', cLon)
        config.set('S1', 'currentLat', cLat)
        config.set('S1', 'direction', direc)
        
        # writing our configuration file
        with open('/home/pi/BerryCache/lastposition.gps', 'w') as configfile:
            config.write(configfile)

    # no signal yet, maybe indoors, use last position from file
    else:
        #read from file
        config.read('/home/pi/BerryCache/lastposition.gps')
        cLon = config.get('S1', 'currentLon')
        cLat = config.get('S1', 'currentLat')
        direc = config.get('S1', 'direction')
        
    return {"lat":cLat, "lon":cLon, "direction":direc}

# Calculates which icon needs to be shown to indicate direction
def direction_icon(lon1,lat1,lon2,lat2,direction):
    direc = float(direction)
    import math
    dy = lat2 - lat1
    dx = lon2 - lon1
    
    angle = math.atan2(dy, dx) * 180 / math.pi
    
    """
    1st q: +
    2nd q: -
    3rd q: +
    4th q: -
    """
    to360 = 0.0
    #B is more north than A
    if dy > 0:   
        if dx > 0:
            # 1st q
            to360 += 90.0 - angle
        else:
            #B is more west than A
            #4th q
            to360 = (3*90.0) + abs(angle)
    #B is more south than A
    else:
        if dx > 0:
            #B is more east than A
            #2nd q
            to360 = 90.0 + abs(angle)
        else:
            #B is more west than A
            #3rd q
            to360 = (2*90.0) + (90.0 - angle)
            
    iconnum = abs(direc - to360) % 360.0

    if 0 <= iconnum <= 45 or 360 >= iconnum > 315:
        #up arrow
        return "\x01"
    if 225 >= iconnum > 135:
        #down arrow
        return "\x07"
    if 135 >= iconnum > 45:
        #right arrow
        return "\x05"
    if 315 >= iconnum > 225:
        #left arrow
        return "\x06"
    

def convertRawGps(data,latlon):
    
    result = ""
    data = float(data)
    
    if data > 0:
        result += "+"
    else:
        result += "-"
    
    # 0 means latitude (-90 to +90)
    if latlon == 0:
        result += str("%.6f" % abs(data)).zfill(9)
        
    # 1 means longitude (-180 to +180)
    if latlon == 1:
        result += str("%.6f" % abs(data)).zfill(10)
        
    return result

class GpsDmn(threading.Thread):
    isTerminated = False
    # Listen on port 2947 (gpsd) of localhost
    session = gps.gps("localhost", "2947")
    
   
    def __init__(self):
        threading.Thread.__init__(self)
        self.session.stream(gps.WATCH_ENABLE | gps.WATCH_NEWSTYLE)
        
    def run(self):
        global currentLat
        global currentLon
        global direction
        
        while not self.isTerminated:
            report = self.session.next()
            # Wait for a 'TPV' report then start writing data to the logger
            if report['class'] == 'TPV':
                # We only need the latitude and longitude, they are in format:
                # +- XX.XXXXXXX for LAT
                # +-XXX.XXXXXXX for LON
                # 7 decimal places gives an accuracy of ~11cm on the equator with a GPS fix
                # Accuracy may be better or worse depending on tall buildings nearby, fog, clouds, or electric interference
                if hasattr(report, 'lat'):
                    currentLat = report.lat
                if hasattr(report, 'lon'):
                    currentLon =  report.lon
                if hasattr(report, 'track'):
                    direction =  report.track
            
    # If called, the while loop in the run() function will terminate
    def terminate(self):
        self.isTerminated = True

import mac 
import checksum
import const
import glob
import routes
import sys
#import serial
import binhex
import singleton
from singleton import SerialPort
# below not needed in final version, just testing
import haversine
import gpsdaemon

# Distance to nearest node in m
nearest = sys.maxint

broadcast = "ffffff"

# Prints the incoming frame to the standard output
def printFrame(prefix,pframe):
    print prefix+"DMC:",pframe[const.dmcBegin:const.dmcEnd]
    print prefix+"SMC:",pframe[const.smcBegin:const.smcEnd]
    print prefix+"NHP:",pframe[const.nhpBegin:const.nhpEnd]
    print prefix+"LON:",pframe[const.lonBegin:const.lonEnd]
    print prefix+"LAT:",pframe[const.latBegin:const.latEnd]
    print prefix+"PRO:",pframe[const.proBegin:const.proEnd]
    print prefix+"SEL:",pframe[const.selBegin:const.selEnd]
    print prefix+"TYP:",pframe[const.typBegin:const.typEnd]
    print prefix+"VER:",pframe[const.verBegin:const.verEnd]
    print prefix+"MET:",pframe[const.metBegin:const.metEnd]
    print prefix+"CID:",pframe[const.cidBegin:const.cidEnd]
    print prefix+"NAM:",pframe[const.nameBegin:const.nameEnd]
    
    if pframe[const.typBegin:const.typEnd] == "03":
        print prefix+"SID:",pframe[const.sidBegin:const.sidEnd]
        print prefix+"Checksum:",pframe[const.chkt3Begin:const.chkt3End]

    if pframe[const.typBegin:const.typEnd] == "04":
        print prefix+"CLON:",pframe[const.clonBegin:const.clonEnd]
        print prefix+"CLAT:",pframe[const.clatBegin:const.clatEnd]
        print prefix+"CDESC:",pframe[const.cdescBegin:const.cdescEnd]
        print prefix+"Checksum:",pframe[const.chkt4Begin:const.chkt4End]
   

# Creates a frame to be sent via RF based on arguments
def createFrame(dmc, nhp, typ, met, ver, pro, cid, lon, lat, sel, name, dict={}):
    # dict is a dictionary of the additional parameters

    # SMC not needed, program can get it
    # END not needed, as it could be changed
    # CHK not needed, it will be calculated
    
    # Type is integer, typ is string
    typ = intToHex(typ)
    type = int(typ, 16)
    
    smc = mac.getHwAddr("eth0")[-6:]
    # Cuts the length to 10 chars, replaces accidentally entered - sign, and justifies to left with - padding
    name = name[:const.nameMax].replace('-','').ljust(const.nameMax,'-')

    final = dmc + smc + nhp + typ + met + ver + pro + cid + lon + lat + sel + name

    if type == 1 or type == 2 or type == 5:
        pass
    if type == 3 or type == 103:
        final += dict['sid']
    if type == 4 or type == 104:
        correct_cdesc = dict['cdesc'][:const.descMax].replace('-','').ljust(const.descMax,'-')
        final += dict['clon'] + dict['clat'] + correct_cdesc
        
    chk = checksum.calculate(final)    
    
    return final + chk + "\n"

# Argument frame should be the string consisting of 75 characters, without the terminating '\n'
def processFrame(frame):
    
    global broadcast
    global nearest
    
    print "\nreceived=",frame
    print "TYPE=",frame[const.typBegin:const.typEnd]
    
    # Type is 1,2,5,6
    # Check for collision at the time of receiption
    if frame[const.typBegin:const.typEnd] == "01" or frame[const.typBegin:const.typEnd] == "02" or frame[const.typBegin:const.typEnd] == "05" or frame[const.typBegin:const.typEnd] == "06":
        if len(frame) != const.frameSizeT12:
            print "The T1/T2/T5/T6 frame is chunk"
            glob.collision = True
            return
        else:
            oldChecksum = frame[const.chkBegin:const.chkEnd]
            if checksum.calculate(frame[:const.chkBegin]) == oldChecksum:
                # checksum is ok, nothing seems to be corrupted
                #print "Checksum is OK"
                pass
            else:
                # frame corrupted, discard
                print "Checksum corrupted - discarding"
                glob.collision = True
                return        
    # Type is 3 or 103
    elif frame[const.typBegin:const.typEnd] == "03" or frame[const.typBegin:const.typEnd] == "67":
        if len(frame) != const.frameSizeT3:
            print "The T3/T103 frame is chunk"
            glob.collision = True
            return
        else:
            oldChecksum = frame[const.chkt3Begin:const.chkt3End]
            if checksum.calculate(frame[:const.chkt3Begin]) == oldChecksum:
                # checksum is ok, nothing seems to be corrupted
                #print "Checksum is OK"
                pass
            else:
                # frame corrupted, discard
                print "Checksum corrupted - discarding"
                glob.collision = True
                return 
    # Type is 4
    elif frame[const.typBegin:const.typEnd] == "04" or frame[const.typBegin:const.typEnd] == "68":
        if len(frame) != const.frameSizeT4:
            print "The T4/T104 frame is chunk"
            glob.collision = True
            return
        else:
            oldChecksum = frame[const.chkt4Begin:const.chkt4End]
            if checksum.calculate(frame[:const.chkt4Begin]) == oldChecksum:
                # checksum is ok, nothing seems to be corrupted
                #print "Checksum is OK"
                pass
            else:
                # frame corrupted, discard
                print "Checksum corrupted - discarding"
                glob.collision = True
                return 
    else:
        # Unkown frame type, probably corrupt frame
        return


    myMac = mac.getHwAddr("eth0")[-6:]

    # are the next hop and destination addresses equal? (last hop or broadcast)
    if frame[const.nhpBegin:const.nhpEnd] == frame[const.dmcBegin:const.dmcEnd]:
        #print "Next hop and destination equals"
        # is it me?
        if frame[const.dmcBegin:const.dmcEnd] == myMac:
            # nhp == dmc == myMac, so destination is me
            # parse data and process them
            
            if frame[const.typBegin:const.typEnd] == "03":
                # Someone wants to initiate connection with me
                # Match profile
                # Show user the popup
                # If accepts, send reply
                # If declines, send type 103 frame (pairing decline)
                
                # First, are we in connect phase?
                if not glob.connectPhase:
                    glob.add_pair(frame)
                    glob.pair_initiator(1)
                    nframe = glob.create_type103()
                    if nframe != None:
                        glob.sendq.put(nframe)
                        glob.send_notify()
                    glob.pair_initiator(-1)
                    glob.clear_pair()
                    return
                
                if glob.paired['status']:
                    if glob.paired['pair'][const.sidBegin:const.sidEnd] == frame[const.sidBegin:const.sidEnd]:
                        #print "received frame from pair - update"
                        pass
                    else:
                        # Actively refuse someone connecting to me, when I'm connected to someone else
                        # Send type 103
                        import ConfigParser
                        
                        coords = gpsdaemon.getlatandlon()
                        cLon = coords['lon']
                        cLat = coords['lat']
                        
                        config = ConfigParser.RawConfigParser()
                        config.read('/home/pi/BerryCache/profiles.pro')
                        profile = config.get('S1', 'myprofile')
                        partner = config.get('S1', 'partnerprofile')
                        
                        fname = config.get('S2', 'name')
                        
                        # Nearest cache ID
                        ncid = "0000000"
                        
                        node = routes.getByMac(frame[const.sidBegin:const.sidEnd])
                        
                        # If we still have route to the destination
                        if node != None:
                            nframe = createFrame(
                                        dmc=node[routes.m],
                                        nhp=node[routes.v],
                                        typ=103,
                                        met='0',
                                        ver='0',
                                        pro=binhex.bintohex(routes.distances[node]['pro']),
                                        cid=ncid,
                                        sel=binhex.bintohex(routes.distances[node]['sel']),
                                        lon=gpsdaemon.convertRawGps(cLon,1),
                                        lat=gpsdaemon.convertRawGps(cLat,0),
                                        name=fname,
                                        dict={'sid':frame[const.dmcBegin:const.dmcEnd]})
                            glob.sendq.put(nframe)
                            return
                        else:
                            return                
                
                import ConfigParser
                config = ConfigParser.RawConfigParser()
                config.read('/home/pi/BerryCache/profiles.pro')
                mypr = config.get('S1', 'myprofile')
                papr = config.get('S1', 'partnerprofile')
                
                # Match the profiles 
                pmatch = 0
                # I match the sender's partner profile
                if binhex.hextobin(frame[const.proBegin:const.proEnd]) == mypr:
                    pmatch += 1
                # The sender's profile matches my partner profile
                if binhex.hextobin(frame[const.selBegin:const.selEnd]) == papr:
                    pmatch += 1
                    
                # Profiles of the neighbours match
                if pmatch == 2:
                    # If connection was initited by me
                    if glob.get_pair_initiator() == 0:
                        # This is a response/update
                        # Add pair data to the memory
                        glob.set_paired_status(True)
                        glob.set_paired_to(frame)
                        # Notify display thread about change
                        glob.notify()
                        # Set display of this message to false
                        glob.t3_req_answer_waiting = False
                    else:
                        # This is a request/pdate
                        # If no pair is connected to me
                        if not glob.get_paired_status():
                            # Add pair data to the memory
                            glob.add_pair(frame)
                            # Initiator was other neighbour
                            glob.pair_initiator(1)
                            # Notify display thread about change
                            glob.notify()       
                        else:
                            # Update
                            if glob.get_paired_to()[const.sidBegin:const.sidEnd] == frame[const.sidBegin:const.sidEnd]:
                                #print glob.get_paired_to()[const.sidBegin:const.sidEnd],"==",frame[const.sidBegin:const.sidEnd]
                                glob.notify()
                                glob.set_paired_to(frame)
                        
                    # Show popup to user to accept/decline the incoming connection
                else:
                    # Profiles do not match (should not happen, because connection can be initiated only when profiles match on the other side
                    # ignore it
                    return
                
                
                
            if frame[const.typBegin:const.typEnd] == "04":
                #print " "
                #printFrame(prefix="/=== ",pframe=frame)
                # Match profile
                
                # First, are we in connect phase?
                if not glob.connectPhase:
                    glob.clear_pair()
                    return
                
                if glob.paired['status']:
                    # We assume they are communicating directly now - hence the comparing with the SMC
                    if glob.paired['pair'][const.sidBegin:const.sidEnd] == frame[const.smcBegin:const.smcEnd]:
                        #print "received frame from pair - cache update"
                        pass
                    else:
                        #print "type 4 received, but not mine?"
                        return
                else:
                    #print "not in paired phase, ignore"
                    return
                
                
                import ConfigParser
                config = ConfigParser.RawConfigParser()
                config.read('/home/pi/BerryCache/profiles.pro')
                mypr = config.get('S1', 'myprofile')
                papr = config.get('S1', 'partnerprofile')
                
                # Match the profiles 
                pmatch = 0
                # I match the sender's partner profile
                if binhex.hextobin(frame[const.proBegin:const.proEnd]) == mypr:
                    pmatch += 1
                # The sender's profile matches my partner profile
                if binhex.hextobin(frame[const.selBegin:const.selEnd]) == papr:
                    pmatch += 1
                    
                # Profiles of the neighbours match
                if pmatch == 2:
                    if glob.last_ignored['cache'] == frame[const.cidBegin:const.cidEnd]:
                        # This cache with this ID was previously ignored
                        # Either by me or the neighbour, do NOT learn it again
                        return
                    glob.found_each_other = True
                    ncache = {'name':frame[const.cdescBegin:const.cdescEnd], 'id':frame[const.cidBegin:const.cidEnd], 'lon':frame[const.clonBegin:const.clonEnd], 'lat':frame[const.clatBegin:const.clatEnd]}
                    glob.cache_dict_add(ncache)
                    glob.notify()
                    return
                else:
                    # Profiles do not match (should not happen, because connection can be initiated only when profiles match on the other side
                    # ignore it
                    return
            
            # Ignored (5)/found (6) cache received
            if frame[const.typBegin:const.typEnd] == "05" or frame[const.typBegin:const.typEnd] == "06":
                import caches
                nc = glob.cache_dict_getnearest()
                if nc == None:
                    return
                ncid = nc['id']
                caches.erase(ncid)
                glob.cache_dict_clear()
                if frame[const.typBegin:const.typEnd] == "06":
                    glob.found_cache = True
                elif frame[const.typBegin:const.typEnd] == "05":
                    glob.ignored_cache = True
                glob.sendq.queue.clear()
                #print "ERASED CACHE, items=",caches.items
                glob.notify()
                return
        
            if frame[const.typBegin:const.typEnd] == "67" or frame[const.typBegin:const.typEnd] == "68":
                # The other node requested me to create a connection
                glob.goto_nb_select()
                glob.cancelled = True
                nearest = sys.maxint
                glob.sendq.queue.clear()
                glob.notify()
                return    
        else:
            # If the destination MAC is broadcast
            if frame[const.dmcBegin:const.dmcEnd] == broadcast:
                print "Broadcast frame"
                # Frame type is RouteDiscovery ( == 1)
                if frame[const.typBegin:const.typEnd] == "01":
                    # Add neighbour as direct, DMC == NHP, metric == 1
                    routes.add(frame)
                # Frame type is RouteAdvertisement (the other device sent data about a node it can see)
                if frame[const.typBegin:const.typEnd] == "02":
                    # If this route is NOT advertising me
                    if frame[const.nhpBegin:const.nhpEnd] != myMac:
                        routes.add(frame)
                    else:
                        return
                
                # dmc:ff, nhp:ff
                coords = gpsdaemon.getlatandlon()
                temp = haversine.haversine(float(frame[const.latBegin:const.latEnd]),float(frame[const.lonBegin:const.lonEnd]),float(coords['lat']),float(coords['lon']))
                if temp < nearest:
                    nearest = temp                    
                    
                import ConfigParser
                config = ConfigParser.RawConfigParser()
                config.read('/home/pi/BerryCache/profiles.pro')
                mypr = config.get('S1', 'myprofile')
                papr = config.get('S1', 'partnerprofile')
                
                # Match the profiles 
                # I match the sender's partner profile
                if binhex.hextobin(frame[const.proBegin:const.proEnd]) == mypr:
                    print frame[const.smcBegin:const.smcEnd],"matches my profile no.",binhex.hextobin(frame[const.proBegin:const.proEnd])
                # The sender's profile matches my partner profile
                if binhex.hextobin(frame[const.selBegin:const.selEnd]) == papr:
                    print myMac,"I match the sender's profile no.",binhex.hextobin(frame[const.proBegin:const.proEnd])
                return
            else:
                # discard frame
                print "Not my frame"
    else:
        # am I the next hop?
        if frame[const.nhpBegin:const.nhpEnd] == myMac:
            if frame[const.dmcBegin:const.dmcEnd] == broadcast:
                if frame[const.typBegin:const.typEnd] == "02":
                    # Someone is advertising me, I have to ignore this
                    return
                return
            
            else:
                # My job is to relay the frame now
                # Replace SMC with my MAC, and NHP with the next on the route to destination
                print "I am the next hop"
                
                # Checksum already calculated, no need to calculate again

                # parse routing table
                # Returns tuple (mac, via)
                parsedAddress = routes.getByMac(frame[const.dmcBegin:const.dmcEnd])
                # Do i have a route to host?
                if frame[const.dmcBegin:const.dmcEnd] == parsedAddress[routes.mac]:
                    print "I have a route to", parsedAddress[routes.mac]
                    # Yes, I do have a route to host
                    newFrame = frame[:const.nhpBegin] + parsedAddress[routes.via] + frame[const.nhpEnd:]
                    newFrame = newFrame[:const.smcBegin] + myMac + newFrame[const.smcEnd:]
                    if frame[const.typBegin:const.typEnd] == "03" or frame[const.typBegin:const.typEnd] == "67":
                        newFrame = newFrame[:const.chkt3Begin] + checksum.calculate(newFrame[:const.chkt3Begin]) + "\n"
                    else:
                        newFrame = newFrame[:const.chkBegin] + checksum.calculate(newFrame[:const.chkBegin]) + "\n"
                    glob.relay_q.put(newFrame)
                else:
                    # no route to host, discard frame
                    print "No route to host", parsedAddress[routes.mac]
        else:
            # I'm not the next hop, discard frame or check if it's RouteAdvertisement
            print "I'm not the next hop"
            # Frame is TYP 2 RouteAdvertisement frame, we'll process the route sent in it
            if frame[const.typBegin:const.typEnd] == "02":
                # The outer if already made sure the route is not advertising me (next hop address is not me)
                # Before adding, the add() function test for duplicates of this route in the routing table                
                routes.add(frame)


def intToHex(integer):
    # convert number to hexadecimal string and cut the first 2 characters: '0x'
    temp = str(hex(integer))[2:]
    if len(temp) < 2:
        temp = ((2 - len(temp)) * "0") + temp
    return temp

def sendFrame(frame):
    ser = singleton.Singleton(SerialPort).getport()

    print "\nSending:",frame
    print "TYPE=",frame[const.typBegin:const.typEnd]

    ser.write(frame)
    ser.close

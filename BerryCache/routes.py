import time
import const
import binhex

table = []
distances = {}

max_item_age = 16
table_change_age = 21

last_table_change_a = []
last_table_change_a.append(time.time())

# Last table change's timestamp
def last_table_change():
    return last_table_change_a[0]

# Touch the table's timer
def update_last_table_change():
    last_table_change_a[0] = time.time()
    
# constants pointing to the data in the entry
m = 0
v = 1
dist = 2

# Function gets attributes from frame
# Called only in case of frame types 1 and 2
def add(frame):
    # Frame type is RouteDiscovery ( == 1)
    if frame[const.typBegin:const.typEnd] == "01":
        # Add neighbour as direct, DMC == NHP, metric == 1
        mac = frame[const.smcBegin:const.smcEnd]
        via = frame[const.smcBegin:const.smcEnd]
        metr = 1
        name = frame[const.nameBegin:const.nameEnd]
    # Frame type is RouteAdvertisement (the other device sent data about a node it can see)
    if frame[const.typBegin:const.typEnd] == "02":
        # We know this route is NOT advertising me, because otherwise function is not called
        mac = frame[const.nhpBegin:const.nhpEnd]
        via = frame[const.smcBegin:const.smcEnd]
        metr = int(frame[const.metBegin:const.metEnd],16)
        name = frame[const.nameBegin:const.nameEnd]

    sel = binhex.hextobin(frame[const.selBegin:const.selEnd])
    pro = binhex.hextobin(frame[const.proBegin:const.proEnd])
    # We have to test if dest mac is in the table, if yes, test if metrics is bigger than current
    # If the destination mac is in the routing table
    # If the metrics to the destination is bigger, than current advertised
    # Remove element from the table
    isPresent = [x for x in table if (x[m] == mac)]
    toRemove = [x for x in table if ((x[m] == mac) and (distances[x]['metrics'] > metr))]
    table[:] = [x for x in table if not ((x[m] == mac) and (distances[x]['metrics'] > metr))]
    # Route exists, but bigger metrics, we have to replace it (first remove, because next hop could change)
    if len(toRemove) != 0:
        # Remove the distance and the element from the dictionary
        del distances[toRemove[0]]
        # Here we add the new item
        t = mac, via
        table.append(t)
        # Add metrics, timestamp and name to the dictionary
        distances[t] = {"metrics":metr,"timestamp":time.time(),"name":name,"sel":sel,"pro":pro}
        update_last_table_change()
        return
    else:
        if len(isPresent) == 0:
            # This item does not exist in the routing table
            # Here we add the new item
            t = mac, via
            table.append(t)
            # Add metrics to the dictionary
            distances[t] = {"metrics":metr,"timestamp":time.time(),"name":name,"sel":sel,"pro":pro}
            update_last_table_change()
            return
        else:
            # This item exists in the routing table, but with smaller OR equal metrics than current
            # So if metrics equal, update timer
            t = mac, via
            if isPresent[0] == t:
                temp = distances[t]["metrics"]
                distances[t] = {"metrics":temp,"timestamp":time.time(),"name":name,"sel":sel,"pro":pro}
                #update_last_table_change()
            # If metrics bigger, do nothing            
            return

# Gets entry from the table based on the MAC as the argument
def getByMac(mac):
    for node in table:
        if node[m] == mac:
            return node
            
# Prints whole routing table for testing
def printTable():
    print "MAC, Via"
    print table
    print "Distance, Time, name, sel, pro"
    print distances
 
# Size of the routing table
def size():
    return len(table)
    
# Erases items from the table, which were not touched in the last max_item_change seconds
def erase_old_entries():
    current = time.time()
    for route in table:
        if (current - distances[route]["timestamp"]) >= max_item_age:
            # Route modified earlier than 16 s, erase it
            # Mac and via
            table.remove(route)
            # Metrics and timestamp to mac and via
            del distances[route]
    

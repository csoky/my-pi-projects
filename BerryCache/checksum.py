def calculate(fromString):
    """
    Original:
    http://stackoverflow.com/questions/16822967/need-assistance-in-calculating-checksum
    
    Calculates checksum for sending commands to the ELKM1.
    Sums the ASCII character values mod256 and takes
    the lower byte of the two's complement of that value.
    """
    return '%0.2X' % (-(sum(ord(c) for c in fromString) % 256) & 0xFF)
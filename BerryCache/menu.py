# Default state of the menu that is shown after a succesful boot
# : delimits the menu categories
# . delimits the last selected object in its category
# Example:
# main.1:profiles.4

menuState = "main.0"

separator = "------------"

main = ["NeighbourFinder",
        "Profiles",
        "Manage caches",
        "GPS",
        "LCD Settings",
        "Shutdown",
        separator]
        
lcdsettings = ["Backlight color",
            "Contrast",
            separator]
        
backlightcolor = ["OFF",
                "Red",
                "Green",
                "Yellow",
                "Blue",
                "Violet",
                "Teal",
                "White",
                separator]

            
managecaches = ["Load",
                "Clear",
                "View caches",
                separator]
                
profiles = ["My profile",
            "Partner profile",
            separator]
            
myprofile = ["Adventurous",
            "Biker",
            "Have a dog",
            "Wheelchair/stroller",
            "Maintenance ready",
            "Climber/hiker",
            "Male/female",
            "Age (u 30)",
            separator]

partnerprofile = ["Adventurous",
                "Biker",
                "Have a dog",
                "Wheelchair/stroller",
                "Maintenance ready",
                "Climber/hiker",
                "Male/female",
                "Age (u 30)",
                separator]


import fcntl, socket, struct

# Function gets MAC address from the interface ifname
# (C) Ben Mackey, 2009
# http://code.activestate.com/recipes/439094-get-the-ip-address-associated-with-a-network-inter/
def getHwAddr(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)    
    info = fcntl.ioctl(s.fileno(), 0x8927,  struct.pack('256s', ifname[:15]))
    return ''.join(['%02x' % ord(char) for char in info[18:24]])
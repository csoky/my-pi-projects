# This module is a container for the parsed XML file of the caches
# After succesful parsing, it contains the cache id : cache name pairs as a dictionary
items = []

def loadcaches():
    import xml.etree.ElementTree as ET
    try:
        # Try to read the XML file
        tree = ET.parse('/home/pi/BerryCache/caches.xml')
        root = tree.getroot()
    except IOError:
        # Indicates error
        return 1
    
    # File was parsed, we'll write its content to the dictionary so we can access it later easier
    for waypoints in root:
        for elems in waypoints:
            if elems.tag == 'name':
                id = elems.attrib['id']
                name = elems.text.replace("\n","")
            if elems.tag == 'coord':
                lat = elems.attrib['lat']
                lon = elems.attrib['lon']
                
                current = {'id':id, 'name':name, 'lat':lat, 'lon':lon}
                items.append(current)
                
    return 0
    

def clearcaches():
    del items[:]
    
def erase(cid):
    for cache in items:
        if cache['id'] == cid:
            items.remove(cache)
    
def werecachesloaded():
    if len(items) == 0:
        return False
    else:
        return True
        

# Based on the current GPS coordinates and the cache database returns the nearest cache
def nearest_cache():
    import gpsdaemon
    import sys
    import haversine
    """
    chcek if db empty
    read gps position or get from device
    traverse array
    apply haversine
    find smallest
    return cid - if its 6 characters long, append "-" to the end
    """
    currentdist = sys.maxint
    bestcache = None
    
    # Caches were not loaded or not available
    if len(items) == 0:
        # Return non-existent cache ID for the caller to identify, if there is an empty database
        return "0000000"
    else:
        # If the caches were already loaded
        # Load current position (either from file or the GPS module)
        coords = gpsdaemon.getlatandlon()
        # For every cache in the cache database
        for cache in items:
            # Traverse the array of caches
            temp = haversine.haversine(float(coords['lon']), float(coords['lat']), float(cache['lon']), float(cache['lat']))
            # If better value found than current
            if temp < currentdist:
                currentdist = temp
                # Set the current cache as active
                bestcache = cache
        
        return bestcache
# Converts 2 characters hexadecimal string to 8 characters binary string array
# The original function:
# http://stackoverflow.com/questions/1425493/convert-hex-to-binary
# by Onedinkenedi, 2011
def hextobin(hex):
    return bin(int(hex, 16))[2:].zfill(8)

# Converts 8 characters binary string to 2 characters hexadecimal number
def bintohex(bin):
    return hex(int(bin, 2))[2:].zfill(2)
    
# In the ptype profile (0 or 1) set the i-th bit to the opposite
def invertbit(ptype,i):
    import ConfigParser
    config = ConfigParser.RawConfigParser()
    config.read('/home/pi/BerryCache/profiles.pro')
    bit = ""
    # My profle
    if ptype == 0:
        mypr = config.get('S1', 'myprofile')
        newmypr = mypr[:i-1] + getinverse(mypr[i-1]) + mypr[i:]
        bit = getinverse(mypr[i-1])
        config.set('S1', 'myprofile', newmypr)
    if ptype == 1:
        papr = config.get('S1', 'partnerprofile')
        newpapr = papr[:i-1] + getinverse(papr[i-1]) + papr[i:]
        bit = getinverse(papr[i-1])
        config.set('S1', 'partnerprofile', newpapr)
    
    # writing our configuration file
    with open('/home/pi/BerryCache/profiles.pro', 'w') as configfile:
        config.write(configfile)
        
    return getinverse(bit)

def getinverse(ch):
    if ch == '0':
        return '1'
    else:
        return '0'

# File contains delimiter values that are used when parsing incoming RF messages
# Numbers represent character positions

dmcBegin=0
dmcEnd=dmcBegin+6

smcBegin=dmcEnd
smcEnd=smcBegin+6

nhpBegin=smcEnd
nhpEnd=nhpBegin+6

typBegin=nhpEnd
typEnd=typBegin+2

metBegin=typEnd
metEnd=metBegin+1

verBegin=metEnd
verEnd=verBegin+1

proBegin=verEnd
proEnd=proBegin+2

cidBegin=proEnd
cidEnd=cidBegin+7

lonBegin=cidEnd
lonEnd=lonBegin+11

latBegin=lonEnd
latEnd=latBegin+10

selBegin=latEnd
selEnd=selBegin+2

# Name max length
nameMax=10
nameBegin=selEnd
nameEnd=nameBegin+nameMax

#-------
#TYPE 1,2 START
chkBegin=nameEnd
chkEnd=chkBegin+2

endBegin=chkEnd
endEnd=endBegin+1
#TYPE 1,2 END
#-------


#--------
#TYPE 3 START
sidBegin=nameEnd
sidEnd=sidBegin+6

chkt3Begin=sidEnd
chkt3End=chkt3Begin+2

endt3Begin=chkt3End
endt3End=endt3Begin+1
#TYPE 3 END
#--------

#--------
#TYPE 4 START
clonBegin=nameEnd
clonEnd=clonBegin+11

clatBegin=clonEnd
clatEnd=clatBegin+10

descMax=40
cdescBegin=clatEnd
cdescEnd=cdescBegin+descMax

chkt4Begin=cdescEnd
chkt4End=chkt4Begin+2

endt4Begin=chkt4End
endt4End=endt4Begin+1
#TYPE 4 END
#--------

frameSize=endEnd
frameSizeT12=endEnd
frameSizeT3=endt3End
frameSizeT4=endt4End
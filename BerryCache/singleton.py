# The original implementation from:
# http://code.activestate.com/recipes/52558-the-singleton-pattern-implemented-with-python/
# by Hrissimir Neikov, 2002

# Include built-in library for handling 
import serial

class SerialPort:
    # Attribute, where the instance is saved
    _instance = None
    # Create serial port pipe
    port = serial.Serial(port="/dev/ttyAMA0",baudrate=9600)

    # Get the class's attribute
    def getport(self):
        return self.port
        
# Function to get the instance of a class
# The argument kl is the name of the class
def Singleton(kl):
    # If class was not instantiated yet
    if not kl._instance:
        # Create an instance and save it to the "pointer" 
        kl._instance = kl()
    # Return the class's instance
    return kl._instance

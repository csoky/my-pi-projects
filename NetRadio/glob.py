# The arrays represent a bitmap of custom characters displayed on the 5*8 dot LCD display
# The escaped hexadecimal numbers are the codes that can be used to print out the special character
# the integers represent the same value as the escaped hex numbers but in decimal
# The RAM of the LCD can only store 8 custom characters at a time
upArrow = [0b00100,
          0b01110,
          0b10101,
          0b00100,
          0b00100,
          0b00100,
          0b00100,
          0b00100]
upArrowN = "\x01"
upArrowNo = 1

menuSelected = [0b11000,
                0b11100,
                0b11110,
                0b11111,
                0b11110,
                0b11100,
                0b11000,
              	0b00000]
menuSelectedN = "\x02"
menuSelectedNo = 2

dash = [0b00100,
	    0b00100,
	    0b00100,
	    0b00100,
	    0b00100,
	    0b00100,
	    0b00100,
	    0b00100]
dashN = "\x03"
dashNo = 3

dot = [0b00000,
        0b01110,
        0b11111,
        0b11111,
        0b11111,
        0b01110,
        0b00000,
        0b00000]
dotN = "\x04"
dotNo = 4

rightArrow = [0b00000,
        0b00100,
        0b00010,
        0b11111,
        0b00010,
        0b00100,
        0b00000,
        0b00000]
rightArrowN = "\x05"
rightArrowNo = 5

leftArrow = [0b00000,
        0b00100,
        0b01000,
        0b11111,
        0b01000,
        0b00100,
        0b00000,
        0b00000]
leftArrowN = "\x06"
leftArrowNo = 6

downArrow = [0b00100,
        0b00100,
        0b00100,
        0b00100,
        0b00100,
        0b10101,
        0b01110,
        0b00100]
downArrowN = "\x07"
downArrowNo = 7

fog = [0b00000,
        0b01010,
        0b10101,
        0b01010,
        0b10101,
        0b01010,
        0b00000,
        0b00000]
fogN = "\x08"
fogNo = 8

volume = [0b00010,
0b01001,
0b00101,
0b10101,
0b10101,
0b00101,
0b01001,
0b00010]
volumeN = "\x09"
volumeNo = 9

import threading
import routes
# Event listener for the print thread in the NfPrint class (handler module)
event = threading.Event()
sendevt = threading.Event()

# LCD "waiting for neighbour..."
t3_req_answer_waiting = False
neighbourFinderActivated = False
# LCD "NAME is joninin. OK? <N|Y>"
showing_pair_req = False
# LCD "Select a partner:"
connectPhase = False
# LCD popup "Neighbour cancelled!"
cancelled = False
# LCD popup "Cache skipped!"
ignored_cache = False
# LCD popup "Cache found!"
found_cache = False
# LCD popup "Route lost!"
route_lost = False
# LCD popup "Did you find the partner?"
near_in_paired_phase = False
# From PairedPhase to CacheFinder phase
found_each_other = False
# LCD popup "Did you find the cache?"
near_in_cache_searching_phase = False
# LCD popup "No more local caches. Waiting."
no_more_local_caches = False
# Parameter to test if collision happened, if set - yes
collision = False
# Send queue time
sendtime = {}
# Default send queu time, can be adjusted
sendtime['time'] = 5.0
# Currently connected to:
paired = {'status': False, 'pair':""}
# All eligible pairs
all_p = []
all_p.append(0)
selected = {}
names = []
pairs = []
pair_init = []
pair_init.append(-1)
# Cache dictionary (negotiated caches, max 2)
cdict = {}
cdict_time = 0
# Ignore updates from last ignored cache
last_ignored = {}
last_ignored['cache'] = ""
neigbour_review = False
# Selected neighbour properties in textual format
props = []
selector = {'num':0}

import Queue
# Sending queue - events
sendq = Queue.Queue()
# Sending queue - type 1,2
route_q = Queue.Queue()
# Sending queue - relayed frames
relay_q = Queue.Queue()

# Parse properties in textual format from binary string
def get_property():
    global props
    import menu
    
    del props[0:len(props)]
    
    # Get partner profile from file
    import ConfigParser
    config = ConfigParser.RawConfigParser()
    config.read('/home/pi/BerryCache/profiles.pro')
    partner = config.get('S1', 'partnerprofile')
    
    for i in range(len(partner)):
        if partner[i] == "1":
            props.append(menu.partnerprofile[i])
    
    return props[selector['num']]

# UP clicked in the neighbour review phase
def up_property():
    global selector
    print "len(props)",len(props)
    selector['num'] = (selector['num'] - 1) % len(props)
    
# DOWN clicked in the neighbour review phase
def down_property():
    global selector
    print "len(props)",len(props)
    selector['num'] = (selector['num'] + 1) % len(props)

# Get send queue time
def get_send_time():
    return sendtime['time']
    
# Set send queue time
def set_send_time(nt):
    sendtime['time'] = nt

# Adjust send queue time randomly <5.0,8.0> sec
# Can not be the same time as before to avoid conflicting again
def adjust_send_time():
    import random
    newtime = float("%.1f" % random.uniform(5.0, 8.0))
    while newtime == get_send_time():
        newtime = float("%.1f" % random.uniform(5.0, 8.0))
    set_send_time(newtime)

# 0 means myself, 1 means neighbour
def pair_initiator(num):
    pair_init[0] = num
    
def get_pair_initiator():
    return pair_init[0]

# Returns all eligible partners before connecting
def all_partners():
    del names[0:len(names)]
    counter = 0
    import ConfigParser
    config = ConfigParser.RawConfigParser()
    config.read('/home/pi/BerryCache/profiles.pro')
    profile = config.get('S1', 'myprofile')
    partner = config.get('S1', 'partnerprofile')
    
    # Count only partners that match my profile, and I match theirs
    for r in routes.table:
        if routes.distances[r]['sel'] == partner and routes.distances[r]['pro'] == profile:
            counter += 1
            names.append(routes.distances[r]['name'].replace("-",""))
            
    all_p[0] = counter
    return all_p[0]

# Sets the neighbours in the structure before connecting
def connect_init():
    all_p[0] = len(routes.table)
    selected['id'] = 0
    if all_partners() == 0:
        selected['name'] = "----------"
    else:
        selected['name'] = names[selected['id']]
    print "\ninit run. values=",selected,all_partners(),"\n"

def select_connect():
    return selected['name']
    
# Returns tuple (mac, via) from the routing table
# Based on the selected user's name
def macvia_from_selected_name():
    import const
    for r in routes.table:
        print "comparing ",routes.distances[r]['name'],"and",selected['name'].ljust(const.nameMax,'-')
        if routes.distances[r]['name'] == selected['name'].ljust(const.nameMax,'-'):
            print "found!"
            return r
    # Name not found
    return None

# UP is clicked in connect phase
def up_connect():
    dividend = all_partners()
    if dividend != 0:
        selected['id'] += 1
        selected['id'] %= dividend
        selected['name'] = names[selected['id']]

# DOWN is clicked in connect phase
def down_connect():
    dividend = all_partners()
    if dividend != 0:
        selected['id'] -= 1
        selected['id'] %= dividend
        selected['name'] = names[selected['id']]
    
# Notify print queue to changed state
def notify():
    event.set()
    event.clear()

# Not used in final version
def send_notify():
    #sendevt.set()
    #sendevt.clear()
    pass

# Adds connecting (or selected) pair to the structure for later use
def add_pair(frame):
    pairs.append(frame)
    
def get_pair():
    if len(pairs) != 0:
        return pairs[-1]
    else:
        return None
        
def len_pair():
    return len(pairs)
    
def clear_pair():
    del pairs[0:len(pairs)]
    
def set_paired_to(pair):
    paired['pair'] = pair
    
def set_paired_status(stat):
    paired['status'] = stat
    
def get_paired_to():
    return paired['pair']
    
def get_paired_status():
    return paired['status']
    
# Creates a frame to be sent via RF based on arguments
def cFrame(dmc, nhp, typ, met, ver, pro, cid, lon, lat, sel, name, dict={}):
    import const
    import mac
    import checksum
    
    # dict is a dictionary of the additional parameters

    # SMC not needed, program can get it
    # END not needed, as it could be changed
    # CHK not needed, it will be calculated
    
    # Type is integer, typ is string
    typ = str(hex(typ))[2:].zfill(2)
    type = int(typ, 16)
    
    smc = mac.getHwAddr("eth0")[-6:]
    # Cuts the length to 10 chars, replaces accidentally entered - sign, and justifies to left with - padding
    name = name[:const.nameMax].replace('-','').ljust(const.nameMax,'-')

    final = dmc + smc + nhp + typ + met + ver + pro + cid + lon + lat + sel + name

    if type == 1 or type == 2:
        pass
    if type == 3 or type == 103:
        final += dict['sid']
    if type == 4 or type == 104:
        correct_cdesc = dict['cdesc'][:const.descMax].replace('-','').ljust(const.descMax,'-')
        final += dict['clon'] + dict['clat'] + correct_cdesc
        
    chk = checksum.calculate(final)    
    
    return final + chk + "\n"
    
# Creates type 3 error frame  
def create_type103():
    import gpsdaemon
    import ConfigParser
    import binhex
    import const
    import caches
    frame = get_pair()

    coords = gpsdaemon.getlatandlon()
    cLon = coords['lon']
    cLat = coords['lat']
    
    config = ConfigParser.RawConfigParser()
    config.read('/home/pi/BerryCache/profiles.pro')
    profile = config.get('S1', 'myprofile')
    partner = config.get('S1', 'partnerprofile')
    
    fname = config.get('S2', 'name')
    
    # Nearest cache ID
    nearc = caches.nearest_cache()
    if nearc != "0000000":
        ncid = nearc['id']
    else:
        return None
    
    # 0 means myself, 1 means neighbour
    initiator = get_pair_initiator()
    if initiator == 1:
        node = routes.getByMac(frame[const.sidBegin:const.sidEnd])

    if initiator == 0:
        node = routes.getByMac(frame[const.dmcBegin:const.dmcEnd])
        
    # If we still have route to the destination
    if node != None:
        nframe = cFrame(
                    dmc=node[routes.m],
                    nhp=node[routes.v],
                    typ=103,
                    met='0',
                    ver='0',
                    pro=binhex.bintohex(routes.distances[node]['pro']),
                    cid=ncid,
                    sel=binhex.bintohex(routes.distances[node]['sel']),
                    lon=gpsdaemon.convertRawGps(cLon,1),
                    lat=gpsdaemon.convertRawGps(cLat,0),
                    name=fname,
                    dict={'sid':frame[const.dmcBegin:const.dmcEnd]})
        return nframe
    else:
        return None
        
# Creates type 4 error frame        
def create_type104():
    import gpsdaemon
    import ConfigParser
    import binhex
    import const
    import glob
    frame = get_pair()

    coords = gpsdaemon.getlatandlon()
    cLon = coords['lon']
    cLat = coords['lat']
    
    config = ConfigParser.RawConfigParser()
    config.read('/home/pi/BerryCache/profiles.pro')
    profile = config.get('S1', 'myprofile')
    partner = config.get('S1', 'partnerprofile')
    
    fname = config.get('S2', 'name')
    
    # Nearest cache ID
    nc = glob.cache_dict_getnearest()
    if nc == None:
        return None
    ncid = nc['id']
    
    # 0 means myself, 1 means neighbour
    initiator = get_pair_initiator()
    if initiator == -1:
        # Probably connection terminated before
        return None
    
    if initiator == 1:
        node = routes.getByMac(frame[const.sidBegin:const.sidEnd])

    if initiator == 0:
        node = routes.getByMac(frame[const.dmcBegin:const.dmcEnd])
        
    # If we still have route to the destination
    if node != None:
        nframe = cFrame(
                    dmc=node[routes.m],
                    nhp=node[routes.v],
                    typ=104,
                    met='0',
                    ver='0',
                    pro=binhex.bintohex(routes.distances[node]['pro']),
                    cid=ncid,
                    sel=binhex.bintohex(routes.distances[node]['sel']),
                    lon=gpsdaemon.convertRawGps(cLon,1),
                    lat=gpsdaemon.convertRawGps(cLat,0),
                    name=fname,
                    dict={'clon':gpsdaemon.convertRawGps(nc['lon'],1), 'clat':gpsdaemon.convertRawGps(nc['lat'],0), 'cdesc':nc['name']})
        return nframe
    else:
        return None
 
# Adds a cache to the cache dicionary (used in negotiation phase)
def cache_dict_add(newcache):
    import time
    global cdict_time
    
    if time.time() - cdict_time > 15:
        return
    else:
        cdict[newcache['id']] = newcache
        cdict_time_update()

# Gets nearest from negotiated caches
def cache_dict_getnearest():
    import caches
    import gpsdaemon
    import haversine
    import time
    global cdict_time
    
    coords = gpsdaemon.getlatandlon()
    cLon = coords['lon']
    cLat = coords['lat']
    
    n = caches.nearest_cache()
    print "in cache_dict_getnearest, n=",n
    print "len cdict=",len(cdict)
    
    # Caches not yet negotiated
    if len(cdict) == 0:
        if n == "0000000":
            # No more local caches left
            no_more_local_caches = True
            t3_req_answer_waiting = False
            connectPhase = False
            cancelled = False
            ignored_cache = False
            found_cache = False
            route_lost = False
            near_in_paired_phase = False
            found_each_other = False
            near_in_cache_searching_phase = False
            cdict_time_update()
            return None
        # Add my nearest only
        cdict[n['id']] = n
        cdict_time_update()
        return cdict[n['id']]
        
    # Caches negotiated and equal
    # OR
    # Caches not yet negotiated, we have just ours/neighbours side
    if len(cdict) == 1:
        try:
            # Only one item in cache dict, and thats my nearest
            if n != "0000000":
                cdict[n['id']]
                return cdict[n['id']]
            else:
                m = cdict[min(cdict)]
                if m == "0000000":
                    return None
                else:
                    return m
        except KeyError:
            if n == "0000000":
                # No more local caches left
                no_more_local_caches = True
                t3_req_answer_waiting = False
                connectPhase = False
                cancelled = False
                ignored_cache = False
                found_cache = False
                route_lost = False
                near_in_paired_phase = False
                found_each_other = False
                near_in_cache_searching_phase = False
                cdict_time_update()
                return None
            if time.time() - cdict_time > 15:
                return cdict[min(cdict)]
            else:
                # Only one item in cache dict, and that's the neigbour's nearest
                # If neighbour's nearest is nearer than mine, follow him
                neighbour_nearest = cdict[min(cdict)]
                neighbour_nearest_dist = haversine.haversine(float(neighbour_nearest['lon']),float(neighbour_nearest['lat']),float(cLon),float(cLat))
                my_nearest_dist = haversine.haversine(float(n['lon']),float(n['lat']),float(cLon),float(cLat))
                cdict[n['id']] = n
                if neighbour_nearest_dist < my_nearest_dist:
                    return neighbour_nearest
                # Else, select the smaller CID
                else:
                    return cdict[min(cdict)]
        
        
    if len(cdict) == 2:
        if n == "0000000":
            # No more local caches left
            no_more_local_caches = True
            t3_req_answer_waiting = False
            connectPhase = False
            cancelled = False
            ignored_cache = False
            found_cache = False
            route_lost = False
            near_in_paired_phase = False
            found_each_other = False
            near_in_cache_searching_phase = False
            cdict_time_update()
            return None
        for key in cdict:
            if key == n['id']:
                my_nearest_dist = haversine.haversine(float(n['lon']),float(n['lat']),float(cLon),float(cLat))
            else:
                neighbour_nearest_dist = haversine.haversine(float(cdict[key]['lon']),float(cdict[key]['lat']),float(cLon),float(cLat))
                neighbour_nearest = cdict[key]
        
        if neighbour_nearest_dist < my_nearest_dist:
            return neighbour_nearest
        # Else, select the smaller CID
        else:
            return cdict[min(cdict)]
    
# Touch cache dictionary update time    
def cdict_time_update():
    global cdict_time
    import time
    
    temp = cdict_time
    cdict_time -= temp
    cdict_time += time.time()
    
    
def cache_dict_clear():
    cdict.clear()
    cdict_time_update()
    
# Returns to the menu   
def all_clear():
    global t3_req_answer_waiting
    global connectPhase
    global cancelled
    global ignored_cache
    global found_cache
    global route_lost
    global near_in_paired_phase
    global found_each_other
    global near_in_cache_searching_phase
    global no_more_local_caches
    global showing_pair_req
    global neighbourFinderActivated
    global neigbour_review
    
    global paired
    paired['status'] = False
    paired['pair'] = ""
    
    global all_p
    del all_p[0:len(all_p)]
    all_p.append(0)
    
    global names
    global pairs
    global pair_init
    del names[0:len(names)]
    del pairs[0:len(pairs)]
    del pair_init[0:len(pair_init)]
    pair_init.append(-1)
    
    global cdict
    cdict.clear()
    
    global last_ignored
    last_ignored['cache'] = ""
    
    cdict_time_update()
    
    global sendq
    sendq.queue.clear()
    
    global props
    del props[0:len(props)]
    
    global selector
    selector['num'] = 0
    
    neigbour_review = False
    t3_req_answer_waiting = False
    connectPhase = False
    cancelled = False
    ignored_cache = False
    found_cache = False
    route_lost = False
    near_in_paired_phase = False
    found_each_other = False
    near_in_cache_searching_phase = False
    no_more_local_caches = False
    neighbourFinderActivated = False
    showing_pair_req = False
 
# Returns to the neighbour finder phase
def goto_nf_activated():
    all_clear()
    global neighbourFinderActivated
    neighbourFinderActivated = True
    global selected
    selected.clear()
    
# Returns to the neighbour select phase   
def goto_nb_select():    
    all_clear()
    global connectPhase
    global neighbourFinderActivated
    connectPhase = True
    neighbourFinderActivated = True
    
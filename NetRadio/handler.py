#!/usr/bin/python
from Adafruit_CharLCDPlate import Adafruit_CharLCDPlate
from time import sleep
import time
#import serial
import threading
import subprocess
import ConfigParser
import sys

# User defined libraries
import menu
import mac
import header
import haversine
import binhex
import const
import routes
import glob
import singleton
import caches
from singleton import SerialPort
from gpsdaemon import GpsDmn # Import the class only
import gpsdaemon # Because variables will be needed
# from gpsdaemon import * can't be used because it pollutes the namespace


# Button definitions
NONE           = 0x00  
SELECT         = 0x01  
RIGHT          = 0x02  
DOWN           = 0x04  
UP             = 0x08  
LEFT           = 0x10  
UP_AND_DOWN    = 0x0C  
LEFT_AND_RIGHT = 0x12
       
# Initialize the display handler
lcd = Adafruit_CharLCDPlate(busnum = 1)
lcd.begin(16, 2)
lcd.clear()

# Create the custom characters defined in glob.py
lcd.createChar(glob.dotNo, glob.dot)
lcd.createChar(glob.dashNo, glob.dash)
lcd.createChar(glob.menuSelectedNo, glob.menuSelected)
lcd.createChar(glob.rightArrowNo, glob.rightArrow)
lcd.createChar(glob.leftArrowNo, glob.leftArrow)
lcd.createChar(glob.downArrowNo, glob.downArrow)
lcd.createChar(glob.upArrowNo, glob.upArrow)
lcd.createChar(glob.fogNo, glob.fog)
lcd.createChar(glob.volumeNo, glob.volume)

gpsActivated = False
isBlocked = False
isShutdown = False
myprofileset = False
partnerprofileset = False
viewcaches_activated = False
viewcaches_id = 0
cache_copy = []
l = 16
name = {}
nstart = 0
nend = nstart + l
wait_count = 0

def get_volume():
    vol = subprocess.check_output("mpc volume; exit 0",stderr=subprocess.STDOUT,shell=True)
    vol = (vol[7:].lstrip())[:-2]
    return vol

def get_station_title():
    v = subprocess.check_output("mpc current; exit 0",stderr=subprocess.STDOUT,shell=True)
    print 'current station: '+v
    return v

# Scrolls long elements in the menu until terminated, or end of string reached
# Terminating can be a buttonpress or an activated function
class LcdScroller(threading.Thread):
    isTerminated = False
    isSuspended = False
    scrollstarted = False
    volume = ""
    othersecond = ""
    firstline = ""
    # passing the LCD as argument
    def __init__(self, dsp):
        threading.Thread.__init__(self)
        self.dsp = dsp
        
    def run(self):
        l = 16
        t = 0.3
        suspCount = 0
        
        while not self.isTerminated:
            if not self.isSuspended:
                # Init, copy the item to the worker
                if not self.scrollstarted:
                    print 'starting scroll'
                    printitem = self.firstline
                    self.scrollstarted = True
                
                # if menu item is longer than l
                if len(printitem) >= l:
                    self.dsp.clear()
                    self.dsp.message(printitem[:l])
                    self.dsp.message("\n" + self.getsecondline())
                    printitem = printitem[1:]
                    sleep(t)
                else:
                    # String shorter or equal than 16, no scrolling needed
                    self.dsp.clear()
                    self.dsp.message(self.firstline[:l])
                    self.dsp.message("\n" + self.getsecondline())
                    self.scrollstarted = False
                    self.isSuspended = True
                # else do nothing
            else:
                sleep(t)
                suspCount += 1
                if suspCount >= 16:
                    suspCount = 0
                    self.isSuspended = False
                    self.firstline = get_station_title()
                    print 'change'
            
    # If called, the while loop in the run() function will terminate
    def terminate(self):
        self.isTerminated = True
    
    # If called, suspends the thread to update the first line of the LCD (probably other functions running)
    def suspend(self):
        print 'suspending?'
        self.isSuspended = True
        self.scrollstarted = False
        
    def allow(self):
        self.isSuspended = False
        
    # Sets the current menu item to be scrolled
    #def setcurrent(self,item):
    #    l = 16
    #    # If string is shorter than 16, add spaces to the end
    #    if len(item) < 16:
    #        item += ((16 - len(item)) * " ")
    #    self.currentitem = item
        
    # Set second line of the LCD to be printed
    def setvolume(self,sl):
        self.volume = sl
        
    def setfirstline(self,fl):
        self.firstline = fl
        
    def getsecondline(self):
        return glob.volumeN + self.volume + self.othersecond
        

scThread = LcdScroller(lcd)
scThread.daemon = True


# Function gets the string and converts it into the menu structure's first line selected element:
# >MenuItem |dot upArrow
# If string length > 16, the element will be truncated
def menuSel(longString):    
    l = 16
    shortenedString = longString[:(l-1)] if len(longString) > (l-1) else longString
    final = shortenedString
    if len(final) < l:
        final = final + ((l - len(final)) * " ")
    
    scThread.setcurrent(longString)
    scThread.allow()
    
    return final

# Function gets the string and converts it into the menu structure's second line selected element
# If string length > 16, the element will be truncated
def menuNext(longString):
    l = 16
    final = longString[:l] if len(longString) > l else longString
    if len(final) < l:
        final = final + ((l - len(final)) * " ")
    scThread.setsecondline(final)
    return final

# Function to read keypresses, even when same key pressed multiple times
def readKeyPressed(): 
    keys = lcd.buttons()
    if(keys != 0):
        while(lcd.buttons() != 0):  
            sleep(0.001)  
    return keys


# Displays a popup on the LCD, message, first, second line and length of the popup given as arguments
def lcdpopup(dsp, msg, first, second, time):
    scThread.suspend()
    dsp.clear()
    dsp.message(msg)
    sleep(time)
    
    dsp.clear()
    dsp.message(menuSel(first))
    dsp.message("\n" + menuNext(second))
    

# Initialize the menu with the default menu.menuState variable
menus = menu.menuState.split(":")
# mainLength is the number of items in the menu we're currently in
mainLength = len(menu.main)
# Selects the upper menu from the hierarchy
selected = menus[0].split(".")
# The item of the menu that will be on the first line of the LCD
sel = int(selected[1])
# The name of the menu we're in, Current Working Directory
cwd = selected[0]

# Display the menu, with the default selected item defined in menu.menuState
#lcd.message(menuSel(eval("menu." + cwd + "[" + str(sel % mainLength) + "]")))
#lcd.message("\n" + menuNext(eval("menu." + cwd + "[" + str((sel + 1) % mainLength) + "]")))

    
gpsActivated = False
glob.neighbourFinderActivated = False
isBlocked = False
isShutdown = False
myprofileset = False
partnerprofileset = False
glob.showing_pair_req = False


subprocess.check_output("mpc load Radios; mpc play; exit 0",stderr=subprocess.STDOUT,shell=True)
scThread.setvolume(get_volume())
scThread.setfirstline(get_station_title())

scThread.start()

    
while True:
    # The loop keeps going forever, reads the keypresses every ~10ms 
    # This approach is necessary because there may (and will) be pressed the same keys repeatedly 
    keyPress = readKeyPressed()

    # The program uses the variable sel to determine which element from the menu is currently selected
    # sel % mainLength gives the exact selected item
    
    if keyPress == DOWN:
        # Suspend the menu item scroller thread, if running
        scThread.suspend()
        
        #lcd.clear()
        
        #volume down
        subprocess.check_output("mpc volume -5; exit 0",stderr=subprocess.STDOUT,shell=True)
        scThread.setvolume(get_volume())
        
        scThread.allow()
        #lcd.message(menuSel(eval("menu." + cwd + "[" + str(sel % mainLength) + "]") + indicator))
        #lcd.message("\n" + menuNext(eval("menu." + cwd + "[" + str((sel + 1) % mainLength) + "]") + indicator2))
    
    if keyPress == UP:
        # Suspend the menu item scroller thread, if running
        scThread.suspend()
        
        #lcd.clear()
        
        #volume up
        subprocess.check_output("mpc volume +5; exit 0",stderr=subprocess.STDOUT,shell=True)
        scThread.setvolume(get_volume())
        
        scThread.allow()
        #lcd.message(menuSel(eval("menu." + cwd + "[" + str(sel % mainLength) + "]") + indicator))
        #lcd.message("\n" + menuNext(eval("menu." + cwd + "[" + str((sel + 1) % mainLength) + "]") + indicator2))
    
    if keyPress == SELECT:
        lcd.clear()
        lcd.backlight(0)
        #turn lcd light off
        subprocess.check_output("sudo service netradio stop; exit 0",stderr=subprocess.STDOUT,shell=True)
    
    if keyPress == RIGHT:
        # Suspend the menu item scroller thread, if running
        scThread.suspend()
        # If the user selects something in the menu, the program tries to find a variable named based on the selected item, 
        # but lowercase and without spaces - no need to keep indexes for all the submenus and items
        # -------------------------------------------------------------------------------------------
        # In special cases, when the user selects an item, that's not in the menu structure as submenu, rather as a function or
        # a standalone application, the program runs that application or function, that was selected
        
        this_clicked = eval("menu." + cwd + "[" + str(sel % mainLength) + "]").replace(" ","").lower()
        # Main menu buttons
        if this_clicked == menu.separator:
            # User clicked on separator, just ignore
            continue
        if this_clicked == "neighbourfinder":
            if not caches.werecachesloaded():
                # Caches were not loaded, display warning
                if caches.loadcaches() == 1:
                    # No such file
                    lcd.clear()
                    lcd.message("File caches.xml\nnot found!")
                    sleep(2)
                    # Update the LCD
                    lcd.clear()
                    lcd.message(menuSel(eval("menu." + cwd + "[" + str(sel % mainLength) + "]")))
                    lcd.message("\n"+menuNext(eval("menu." + cwd + "[" + str((sel+1) % mainLength) + "]"))) 
                    continue
                
                print caches.items
                lcd.clear()
                if len(caches.items) != 0:
                    lcd.message("Caches.xml...\n")
                    lcd.message("Loaded!".center(16," "))
                else:
                    lcd.message("Caches.xml...\n")
                    lcd.message("Not found!".center(16," "))
                sleep(1)
                # Update the LCD
                lcd.clear()
                lcd.message(menuSel(eval("menu." + cwd + "[" + str(sel % mainLength) + "]")))
                lcd.message("\n"+menuNext(eval("menu." + cwd + "[" + str((sel+1) % mainLength) + "]")))
                # Erase following line after finalizing
                scThread.suspend()
            if not glob.neighbourFinderActivated:
                glob.neighbourFinderActivated = True
            else:
                if glob.connectPhase:
                    if glob.near_in_cache_searching_phase:
                        # Found the current cache, send a TYPE 6 (cache found) frame
                        glob.near_in_cache_searching_phase = False
                        macvia = glob.macvia_from_selected_name()
                        if macvia != None:
                            coords = gpsdaemon.getlatandlon()
                            cLon = coords['lon']
                            cLat = coords['lat']
                            
                            config = ConfigParser.RawConfigParser()
                            config.read('/home/pi/BerryCache/profiles.pro')
                            profile = config.get('S1', 'myprofile')
                            partner = config.get('S1', 'partnerprofile')
                            
                            nc = glob.cache_dict_getnearest()
                            
                            if nc == None:
                                glob.no_more_local_caches = True
                                continue
                            
                            ncid = nc['id']
                            glob.last_ignored['cache'] = ncid
                            
                            fname = config.get('S2', 'name')
                        
                            nframe = header.createFrame(
                                dmc=macvia[routes.m],
                                nhp=macvia[routes.v],
                                typ=6,
                                met='0',
                                ver='0',
                                pro=binhex.bintohex(partner),
                                cid=ncid,
                                sel=binhex.bintohex(profile),
                                lon=gpsdaemon.convertRawGps(cLon,1),
                                lat=gpsdaemon.convertRawGps(cLat,0),
                                name=fname)
                                
                            # Ignore the current cache
                            caches.erase(ncid)
                            glob.cache_dict_clear()
                            glob.found_cache = True
                            glob.notify()
                            glob.sendq.queue.clear()
                            glob.sendq.put(nframe)
                            glob.send_notify()
                            continue
                        else:
                            # no route to destination, probably lost or shutted down
                            glob.route_lost = True
                            glob.goto_nb_select()
                            caches.erase(glob.cache_dict_getnearest()['id'])
                            glob.notify()
                            continue
                    if glob.found_each_other:
                        if nend == len(name['id'])+1:
                            nend = l
                            nstart = 0
                        else:
                            nstart += 1
                            nend += 1
                        glob.notify()
                        continue
                    if glob.near_in_paired_phase:
                        # Found the other one who is connected, begin sending TYPE 4 frame
                        glob.found_each_other = True
                        glob.near_in_paired_phase = False
                        
                        # Tuple (mac, via)
                        macvia = glob.macvia_from_selected_name()
                        
                        if macvia != None:
                            coords = gpsdaemon.getlatandlon()
                            cLon = coords['lon']
                            cLat = coords['lat']
                            
                            config = ConfigParser.RawConfigParser()
                            config.read('/home/pi/BerryCache/profiles.pro')
                            profile = config.get('S1', 'myprofile')
                            partner = config.get('S1', 'partnerprofile')
                            
                            fname = config.get('S2', 'name')
                            
                            # Nearest cache ID
                            ncache = glob.cache_dict_getnearest()
                            if ncache == None:
                                glob.no_more_local_caches = True
                                continue
                            ncid = ncache['id']
                            
                            nframe = header.createFrame(
                                dmc=macvia[routes.m],
                                nhp=macvia[routes.v],
                                typ=4,
                                met='0',
                                ver='0',
                                pro=binhex.bintohex(partner),
                                cid=ncid,
                                sel=binhex.bintohex(profile),
                                lon=gpsdaemon.convertRawGps(cLon,1),
                                lat=gpsdaemon.convertRawGps(cLat,0),
                                name=fname,
                                dict={'clon':gpsdaemon.convertRawGps(ncache['lon'],1),'clat':gpsdaemon.convertRawGps(ncache['lat'],0),'cdesc':ncache['name']})

                            glob.sendq.put(nframe)
                            glob.send_notify()
                            glob.notify()
                        else:
                            # no route to destination, probably lost or shutted down
                            glob.route_lost = True
                            glob.goto_nb_select()
                            glob.notify()
                        continue
                        
                    if glob.showing_pair_req:
                        glob.showing_pair_req = False
                        glob.set_paired_status(True)
                        # Whole frame
                        glob.set_paired_to(glob.get_pair())
                        glob.notify()
                    # Create frame
                    # Send type 3 request to selected neighbour
                    
                    if not glob.neigbour_review:
                        glob.neigbour_review = True
                        glob.notify()
                        continue
                        
                    if glob.neigbour_review:
                        glob.neigbour_review = False
                        glob.t3_req_answer_waiting = True
                        glob.notify()
                    
                    # Tuple (mac, via)
                    macvia = glob.macvia_from_selected_name()
                    
                    if macvia != None:
                        coords = gpsdaemon.getlatandlon()
                        cLon = coords['lon']
                        cLat = coords['lat']
                        
                        config = ConfigParser.RawConfigParser()
                        config.read('/home/pi/BerryCache/profiles.pro')
                        profile = config.get('S1', 'myprofile')
                        partner = config.get('S1', 'partnerprofile')
                        
                        fname = config.get('S2', 'name')
                        
                        # Nearest cache ID
                        ncache = caches.nearest_cache()
                        if ncache == "0000000":
                            glob.no_more_local_caches = True
                            continue
                        ncid = ncache['id']
                        
                        nframe = header.createFrame(
                            dmc=macvia[routes.m],
                            nhp=macvia[routes.v],
                            typ=3,
                            met='0',
                            ver='0',
                            pro=binhex.bintohex(partner),
                            cid=ncid,
                            sel=binhex.bintohex(profile),
                            lon=gpsdaemon.convertRawGps(cLon,1),
                            lat=gpsdaemon.convertRawGps(cLat,0),
                            name=fname,
                            dict={'sid':mac.getHwAddr("eth0")[-6:]})
                        if glob.get_paired_status():
                            glob.sendq.put(nframe)
                            glob.send_notify()
                        else:
                            glob.pair_initiator(0)
                            glob.sendq.put(nframe)
                            glob.send_notify()
                            glob.add_pair(nframe)
                            glob.t3_req_answer_waiting = True
                            glob.notify()
                    else:
                        # no route to destination, probably lost or shutted down
                        glob.route_lost = True
                        glob.goto_nb_select()
                        glob.notify()
                continue
            sendThread = SendData()
            sendThread.daemon = True
            sendThread.start()
            # Printer thread to show no. of found neighbours
            nfPrintThread = NfPrint(lcd)
            nfPrintThread.daemon = True
            nfPrintThread.start()
            isBlocked = True
            continue
        if this_clicked == "gps":
            # Start the thread, that updates the GPS coordinates on the LCD every second
            if not gpsActivated:
                gpsPrint = GpsPrint(lcd)
                gpsPrint.daemon = True
                gpsPrint.start()
                gpsActivated = True
                isBlocked = True
            continue
        if this_clicked == "shutdown":
            # Replace last menu's position indicator number with current
            lcd.clear()
            lcd.message("Are you sure?\n")
            lcd.message("Yes " + glob.upArrowN + " | No " + glob.downArrowN)
            isBlocked = True
            isShutdown = True            
            continue
        
        # Submenu buttons
        if cwd == "myprofile":
            myprofileset = True
            
        if cwd == "partnerprofile":
            partnerprofileset = True
            
        # If any of the profile options was clicked
        try:
            index = ["adventurous","biker","haveadog","wheelchair/stroller","maintenanceready","climber/hiker","male/female","age(u30)"].index(this_clicked)
        except ValueError:
            pass
        else:
            ret = ""
            # Invert selected bit in the profile binary string
            if myprofileset:
                ret = binhex.invertbit(0,index+1)
            if partnerprofileset:
                ret = binhex.invertbit(1,index+1)
            
            message = ""
            # Print what's happened
            if ret == "0":
                message = "SET".center(16," ")
            else:
                message = "UNSET".center(16," ")

            import ConfigParser
            config = ConfigParser.RawConfigParser()
            config.read('/home/pi/BerryCache/profiles.pro')
            read_myprofile = config.get('S1', 'myprofile')
            read_partnerprofile = config.get('S1', 'partnerprofile')
        
            if cwd == "myprofile":
                if read_myprofile[sel % (mainLength-1)] == "1":
                    indicator = glob.dotN
                elif read_myprofile[sel % (mainLength-1)] == "0":
                    indicator = glob.fogN
                else:
                    indicator = ""
                 
                if read_myprofile[(sel+1) % (mainLength-1)] == "1":
                    indicator2 = glob.dotN
                elif read_myprofile[(sel+1) % (mainLength-1)] == "0":
                    indicator2 = glob.fogN
                else:
                    indicator2 = ""
                
            if cwd == "partnerprofile":
                if read_partnerprofile[sel % (mainLength-1)] == "1":
                    indicator = glob.dotN
                elif read_partnerprofile[sel % (mainLength-1)] == "0":
                    indicator = glob.fogN
                else:
                    indicator = ""
                    
                if read_myprofile[(sel+1) % (mainLength-1)] == "1":
                    indicator2 = glob.dotN
                elif read_myprofile[(sel+1) % (mainLength-1)] == "0":
                    indicator2 = glob.fogN
                else:
                    indicator2 = ""

            # Update the LCD
            lcdpopup(dsp=lcd, msg=message, first=eval("menu." + cwd + "[" + str(sel % mainLength) + "]") + indicator, second=eval("menu." + cwd + "[" + str((sel+1) % mainLength) + "]") + indicator2, time=1)
            continue
        
        # Try to open file caches.xml in the program directory and load caches
        # Print the output of the operation to the LCD
        if this_clicked == "load":
            if caches.werecachesloaded():
                lcd.clear()
                lcd.message("Already loaded!")
                sleep(1)
                # Update the LCD
                lcd.clear()
                lcd.message(menuSel(eval("menu." + cwd + "[" + str(sel % mainLength) + "]")))
                lcd.message("\n"+menuNext(eval("menu." + cwd + "[" + str((sel+1) % mainLength) + "]"))) 
            if caches.loadcaches() == 1:
                # No such file
                lcd.clear()
                lcd.message("File caches.xml\nnot found!")
                sleep(2)
                # Update the LCD
                lcd.clear()
                lcd.message(menuSel(eval("menu." + cwd + "[" + str(sel % mainLength) + "]")))
                lcd.message("\n"+menuNext(eval("menu." + cwd + "[" + str((sel+1) % mainLength) + "]"))) 
                continue
            
            print caches.items
            lcd.clear()
            if len(caches.items) != 0:
                lcd.message("Caches.xml...\n")
                lcd.message("Loaded!".center(16," "))
            else:
                lcd.message("Caches.xml...\n")
                lcd.message("Not found!".center(16," "))
            sleep(1)
            # Update the LCD
            lcd.clear()
            lcd.message(menuSel(eval("menu." + cwd + "[" + str(sel % mainLength) + "]")))
            lcd.message("\n"+menuNext(eval("menu." + cwd + "[" + str((sel+1) % mainLength) + "]")))
            continue
        
        # Reset the loaded caches, clear the structure that is saving them
        if this_clicked == "clear":
            caches.clearcaches()
            lcd.clear()
            if len(caches.items) == 0:
                lcd.message("Cleared!".center(16," "))
            else:
                lcd.message("Not cleared!".center(16," "))
            sleep(1)
            # Update the LCD
            lcd.clear()
            lcd.message(menuSel(eval("menu." + cwd + "[" + str(sel % mainLength) + "]")))
            lcd.message("\n"+menuNext(eval("menu." + cwd + "[" + str((sel+1) % mainLength) + "]")))
            continue
        
        if this_clicked == "viewcaches":
            if len(caches.items) == 0:
                lcd.clear()
                lcd.message("Not loaded!".center(16," "))
                sleep(1)
                lcd.clear()
                lcd.message(menuSel(eval("menu." + cwd + "[" + str(sel % mainLength) + "]")))
                lcd.message("\n"+menuNext(eval("menu." + cwd + "[" + str((sel+1) % mainLength) + "]")))
            else:
                if viewcaches_activated:
                    continue
                else:
                    menu.menuState += ":managecaches.2"
                    viewcaches_activated = True
                    cache_copy = list(caches.items)
                    cache_copy.append(menu.separator)
                    
                    menuSel(cache_copy[viewcaches_id]['name'] + "," + cache_copy[viewcaches_id]['id'] + "," + cache_copy[viewcaches_id]['lat'] + "," + cache_copy[viewcaches_id]['lon'])
                    if cache_copy[(viewcaches_id+1)%len(cache_copy)] != menu.separator:
                        menuNext(cache_copy[(viewcaches_id+1)%len(cache_copy)]['name'] + "," + cache_copy[(viewcaches_id+1)%len(cache_copy)]['id'] + "," + cache_copy[(viewcaches_id+1)%len(cache_copy)]['lat'] + "," + cache_copy[(viewcaches_id+1)%len(cache_copy)]['lon'])
                    else:
                        menuNext(menu.separator)
            continue
        
        if cwd == "backlightcolor":
            lcd.backlight(sel % mainLength)
            continue
        if this_clicked == "contrast":
            # Replace last menu's position indicator number with current number
            menu.menuState = menu.menuState + ":lcdsettings.1"
            lcd.clear()
            lcd.message("You can adjust\n")
            lcd.message("/w this" + glob.downArrowN + " button!")
            isBlocked = True
            continue
        
        # Function does not exist
        # If the program flow reaches this snippet, probably no function exists with this name
        # For testing, function not implemented right now
        if not hasattr(menu, this_clicked):
            lcd.clear()
            lcd.message("Not implemented!\n")
            sleep(1)
            # Update the LCD
            lcd.clear()
            lcd.message(menuSel(eval("menu." + cwd + "[" + str(sel % mainLength) + "]")))
            lcd.message("\n"+menuNext(eval("menu." + cwd + "[" + str((sel+1) % mainLength) + "]"))) 
            continue
        
        if cwd == "main":
            menu.menuState = "main." + str(sel % mainLength)
        else:
            menu.menuState = menu.menuState + ":" + cwd + "." + str(sel % mainLength)
            
        cwd = this_clicked
        mainLength = int(eval("len(menu." + cwd + ")"))
        lcd.clear()
        sel = 0

        indicator = ""
        indicator2 = ""
        
        if cwd == "myprofile" or cwd == "partnerprofile":
            import ConfigParser
            config = ConfigParser.RawConfigParser()
            config.read('/home/pi/BerryCache/profiles.pro')
            read_myprofile = config.get('S1', 'myprofile')
            read_partnerprofile = config.get('S1', 'partnerprofile')
        
            if cwd == "myprofile":
                if read_myprofile[sel % (mainLength-1)] == "1":
                    indicator = glob.dotN
                elif read_myprofile[sel % (mainLength-1)] == "0":
                    indicator = glob.fogN
                else:
                    indicator = ""
                 
                if read_myprofile[(sel+1) % (mainLength-1)] == "1":
                    indicator2 = glob.dotN
                elif read_myprofile[(sel+1) % (mainLength-1)] == "0":
                    indicator2 = glob.fogN
                else:
                    indicator2 = ""
                
            if cwd == "partnerprofile":
                if read_partnerprofile[sel % (mainLength-1)] == "1":
                    indicator = glob.dotN
                elif read_partnerprofile[sel % (mainLength-1)] == "0":
                    indicator = glob.fogN
                else:
                    indicator = ""
                    
                if read_myprofile[(sel+1) % (mainLength-1)] == "1":
                    indicator2 = glob.dotN
                elif read_myprofile[(sel+1) % (mainLength-1)] == "0":
                    indicator2 = glob.fogN
                else:
                    indicator2 = ""
            
        
        lcd.message(menuSel(eval("menu." + cwd + "[" + str(sel % mainLength) + "]") + indicator))
        lcd.message("\n" + menuNext(eval("menu." + cwd + "[" + str((sel + 1) % mainLength) + "]") + indicator2))
    
    # A back in the menu - jumps up one level in the tree hierarchy based on menu.menuState
    if keyPress == LEFT:
        # Suspend the menu item scroller thread, if running
        scThread.suspend()
        
        isBlocked = False
        if gpsActivated:
            gpsPrint.terminate()
            gpsActivated = False
        
        if viewcaches_activated:
            viewcaches_activated = False
            
        if glob.neighbourFinderActivated:
            if glob.connectPhase:
                if glob.near_in_cache_searching_phase:
                    # User says he didn't find the cache yet
                    glob.near_in_cache_searching_phase = False
                    glob.notify()
                    continue
                if glob.found_each_other:
                    if nstart != 0 and nend != l:
                        nstart -= 1
                        nend -= 1
                        glob.notify()
                        continue
                    # Cancel the connection
                    # Send type 104
                    nframe = glob.create_type104()
                    print "type104:",nframe
                    # While navigating to the nearest cache
                    # cache ignore will be DOWN
                    glob.goto_nb_select()
                    glob.cancelled = True
                    if nframe != None:
                        glob.sendq.put(nframe)
                        glob.send_notify()
                    glob.notify()
                    continue
                if glob.no_more_local_caches:
                    # Cancel the connection
                    # Send type 103
                    nframe = glob.create_type103()
                    print "type103:",nframe
                    glob.goto_nf_select()
                    if nframe != None:
                        glob.sendq.put(nframe)
                        glob.send_notify()
                    glob.notify()
                    continue
                if glob.near_in_paired_phase:
                    # User says he didn't find the other one
                    glob.near_in_paired_phase = False
                    glob.notify()
                    continue
                if glob.get_paired_status():
                    # Cancel the connection
                    # Send type 103
                    nframe = glob.create_type103()
                    print "type103:",nframe
                    glob.goto_nf_select()
                    glob.cancelled = True
                    if nframe != None:
                        glob.sendq.put(nframe)
                        glob.send_notify()
                    glob.notify()
                    continue
                if glob.t3_req_answer_waiting:
                    glob.goto_nb_select()
                    glob.cancelled = True
                    glob.notify()
                    continue
                if glob.neigbour_review:
                    glob.neigbour_review = False
                    glob.notify()
                    continue
                if glob.showing_pair_req:
                    glob.showing_pair_req = False
                    # create and send type 103 frame
                    nframe = glob.create_type103()
                    glob.goto_nb_select()
                    if nframe != None:             
                        glob.sendq.put(nframe)
                        glob.send_notify()
                    glob.notify()
                    continue
                glob.goto_nf_activated()
                routes.update_last_table_change()
                glob.notify()
                continue
            else:
                nfPrintThread.terminate()
                sendThread.terminate()
                glob.all_clear()
                glob.selected.clear()
        
        if myprofileset:
            myprofileset = False
        
        if partnerprofileset:
            partnerprofileset = False
        
        menus = menu.menuState.split(":")
        
        # If the main menu is active, we shouldn't go back
        if menus[-1] == "":
            # Update the LCD
            lcd.clear()
            lcd.message(menuSel(eval("menu." + cwd + "[" + str(sel % mainLength) + "]")))
            lcd.message("\n"+menuNext(eval("menu." + cwd + "[" + str((sel+1) % mainLength) + "]")))    
            continue
        
        # Combine the menu breadcrumb again without the last element
        menu.menuState = ":".join(menus[:-1])
        # Selected will contain the name of the last element and the position in it when select/right was pressed last time
        selected = menus[len(menus) - 1].split(".")
        # Number of submenus in the selected menu
        mainLength = eval("len(menu." + selected[0] + ")")
        
        # Set current working menu dir and selected item in it
        sel = int(selected[1])
        cwd = selected[0]
        # Update the LCD
        lcd.clear()
        lcd.message(menuSel(eval("menu." + cwd + "[" + str(sel % mainLength) + "]")))
        lcd.message("\n"+menuNext(eval("menu." + cwd + "[" + str((sel+1) % mainLength) + "]")))
    # Put thread to sleep for 99 ms, then try to retrieve keypress  
    sleep(0.099)
